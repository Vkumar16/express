const express = require('express')
const bodyParser = require('body-parser')
const config = require("./config")
const fs = require('fs')
const path = require('path')
const axios = require('axios')
const { response } = require('express')

require('dotenv').config()

const app = express()
const PORT = config.PORT || 3000


const read = (path) => {
    return new Promise((resolve, reject) => {
        fs.readFile(path, 'utf-8', (error, data) => {
            if (error) {
                reject(error)
            } else {
                resolve(data)
            }
        })
    })
}

app.get('/', (req, res) => {
    res.send("<h1>Hello vishal I am you friend Manish</h1>")
})

app.get('/read', async (req, res, next) => {

    let result = {}
    axios.get('https://api.publicapis.org/entries')
        .then(response => {
            result["First-Data"] = response.data
            return axios.get('https://api.coindesk.com/v1/bpi/currentprice.json')
        })
        .then(response => {
            result["Second-Data"] = response.data
            return axios.get('https://www.boredapi.com/api/activity')
        })
        .then(response => {
            result["Third-Data"] = response.data
            return axios.get('https://api.agify.io/?name=meelad')
        })
        .then(response => {
            result["Fourth-Data"] = response.data
            return result
        })
        .then(result => res.status(200).json({
            data: result
        }))
        .catch(error => {
            const myError = new Error(error)
            myError.status = 500
            next(myError)
        })
})

app.use((req, res, next) => {
    const myError = new Error("404 Error, Page not found")
    myError.status = 404
    next(myError)
})

app.use((error, req, res, next) => {
    res.status(error.status).send({
        message: error.message
    })
})

app.listen(PORT, () => {
    console.log("You are listining port", PORT)
})