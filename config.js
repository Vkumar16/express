require('dotenv').config()

module.exports = {
    dbConnection : {
        host : process.env.HOST,
        dbName : process.env.DB_NAME,
        password : process.env.PASS
    },
    PORT : process.env.PORT
}